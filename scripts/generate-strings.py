#!/usr/bin/python3
# -*- coding: utf-8 -*-
import logging
from libdata import readFolder, dataToFile, getPacks
from statuses import *
import hashlib
import functools
import os
import datetime
import re
import json

ROOT = "../"
FOLDER = "strings/"
inline_re = re.compile("(@[a-zA-Z0-9.]+\[[^\]]+\])(?:({[^}]+}))?")

packs = getPacks()
strings_folder_path = "%s%s" % (ROOT, FOLDER)
if not os.path.exists(strings_folder_path): os.makedirs(strings_folder_path)
generation_time = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
replaces = {}
def field_to_strings_in(valueEN, valueES):
  strings = []
  if isinstance(valueEN , dict) and isinstance(valueES, dict):
    for k in valueEN:
      if k in valueES:
        strings.extend(field_to_strings_in(valueEN[k], valueES[k]))
  elif isinstance(valueEN, (tuple, list, set)) and isinstance(valueES, (tuple, list, set)) and len(valueES) == 0:
    for k in valueEN:
      strings.extend(field_to_strings_in(k, ""))
  elif (valueEN is not None and valueEN != "") and (valueES is None or valueES == ""):
    strings.append(valueEN)
  return strings

def exctract_strings_of(entry):
  fields = ["name", "descr", "lists", "data"]
  strings = []
  for f in fields:
    if f + "EN" in entry and f + "ES" in entry:
      strings.extend(field_to_strings_in(entry[f + "EN"], entry[f + "ES"]))
  return strings

char_count = 0
page = 1

def my_replace(match):
    global replaces
    rep = match.group(1)
    md5 = hashlib.md5(rep.encode('utf-8')).hexdigest()
    replaces[md5] = rep
    return "###" + md5 + "###" + (match.group(2) or "XXX") + "###"

def parse(exising):
  global strings_folder_path, generation_time, page, char_count
  for id in existing:
    entry = existing[id]
    if "status" in entry and entry["status"] in [ST_LIBRE, ST_OFICIAL, ST_DUPLICADA]:
      continue
    strings = exctract_strings_of(entry)
    length = functools.reduce(lambda a, b: a + b + 35, map(len, strings), 0)
    if char_count + length > 1000000:
      page += 1
      print (f'Generating new page: {page} {char_count} {length}')
      char_count = length
    else:
      char_count = char_count + length
    filename = "%s%s_%s.txt" % (strings_folder_path, generation_time, str(page).zfill(2))

    with open(filename, "a") as file:
      for string in strings:
        md5 = hashlib.md5(string.encode('utf-8')).hexdigest()
        file.write(md5 + ":\n")
        file.write(inline_re.sub(my_replace,string) + "\n")

print (f'Generating strings: {generation_time}')
print (f'Generating new page: {page}')
for p in packs:
  print (f'Generating strings for pack: {p["id"]}')
  folderData = readFolder("%sdata/%s/" % (ROOT, p["id"]))
  existing = folderData[0]
  existingByName = folderData[1]
  parse(existing)

  ## items
  if "items" in p:
    print (f'Generating strings for pack: {p["id"]+"-items"}')
    folderData = readFolder("%sdata/%s/" % (ROOT, p["id"]+"-items"))
    existing = folderData[0]
    existingByName = folderData[1]
    parse(existing)

  ## pages
  if "pages" in p:
    print (f'Generating strings for pack: {p["id"]+"-pages"}')
    folderData = readFolder("%sdata/%s/" % (ROOT, p["id"]+"-pages"))
    existing = folderData[0]
    existingByName = folderData[1]
    parse(existing)
print (f'Generating replaces JSON')
with open(strings_folder_path + "replaces.json", 'w') as outfile:
    json.dump(replaces, outfile)