#!/usr/bin/python3
# -*- coding: utf-8 -*-
from libdata import readFolder, dataToFile, getPacks
from statuses import *
import hashlib
import json
import glob
import os
import re
import json

ROOT = "../"
inline_re = re.compile("#+([a-f0-9]{32})#+(?:XXX|([^#]+)?)?#+")
corrct_re = re.compile("√.")

def my_correct(match):
  m = match.group(0)
  if m =="√©":
    return "é"
  elif m =="√°":
    return "á"
  elif m =="√≠":
    return "í"
  elif m =="√≥":
    return "ó"
  elif m =="√∂":
    return "ö"
  elif m =="√º":
    return "ü"
  elif m =="√¥":
    return "ô"
  elif m =="√®":
    return "è"
  elif m =="√ß":
    return "ç"
  elif m =="√±":
    return "ñ"
  elif m =="√∏":
    return "ø"
  elif m =="√´":
    return "ë"
  elif m =="√§":
    return "ä"
  elif m =="√•":
    return "å"
  elif m =="√Å":
    return "Á"
  elif m =="√∫":
    return "ú"
  elif m =="√ª":
    return "û"
  elif m =="√Ø":
    return "ï"
  elif m =="√â":
    return "É"
  elif m =="√†":
    return "à"
  elif m =="√¶":
    return "æ"
  elif m =="√Æ":
    return "î"
  elif m =="√¢":
    return "â"
  elif m =="√£":
    return "ã"
  elif m =="√î":
    return "Ô"
  elif m =="√ü":
    return "ß"
  elif m =="√ì":
    return "Ó"
  elif m =="√≤":
    return "ò"
  elif m =="√Ω":
    return "ý"
  elif m =="√ñ":
    return "Ö"
  elif m =="√™":
    return "ê"
  elif m =="√Ä":
    return "À"
  elif m =="√ò":
    return "Ø"
  elif m =="√Ö":
    return "Å"
  elif m =="√∞":
    return "ð"
  elif m =="√á":
    return "Ç"
  elif m =="√Ç":
    return "Â"
  elif m =="√π":
    return "ù"
  elif m =="√í":
    return "Ò"
  elif m =="√¨":
    return "ì"
  elif m =="√ú":
    return "Ü"
  elif m =="√à":
    return "È"
  elif m =="√û":
    return "Þ"

def load_strings_file(path):
  identifier_re = re.compile("^([a-fA-F\d]{32}):$")
  identifier = None
  f = open(path, "r")
  strings = {}
  for i, line in enumerate(f):
    id = re.search(identifier_re, line)
    identifier = id.group(1) if id is not None else identifier
    if id is None:
      strings[identifier] = line.strip("\n") if strings[identifier] == "" else strings[identifier] + "\n" + line.strip("\n")
    else:
      strings[identifier] = ""
  return strings

def load_strings(path):
  strings = {}
  for file in glob.glob(string_files_path):
    print("Loading strings from %s" % file)
    strings.update(load_strings_file(file))
  return strings

def my_replace(match):
    global replaces
    md5 = match.group(1)
    label = match.group(2) or ""
    if md5 in replaces:
      return "%s%s" % (replaces[md5], label)
    return match.group(0)

def trans(data):
    global strings
    md5Id = hashlib.md5(data.encode('utf-8')).hexdigest()
    return inline_re.sub(my_replace, corrct_re.sub(my_correct,strings[md5Id])) if md5Id in strings else None

def transL(data):
    global strings
    if data is dict:
        for k in data:
            t = transL(data[k])
            if t is not None:
              data[k] = t
            else:
              return None
        return data
    elif isinstance(data, (tuple, list, set)):
        t = list(map(trans, data))
        if None in t:
          return None
        return t
    else:
        return trans(data)

def generate(pack_id):
    print (f'Generating {pack_id}')
    folderData = readFolder("%sdata/%s/" % (ROOT, pack_id))
    existing = folderData[0]
    existingByName = folderData[1]

    # ========================
    # create or update entries
    # ========================
    for id in existing:
        updated = False
        filename = existing[id]["filename"]
        filepath = "%sdata/%s/%s" % (ROOT, pack_id, filename)
        if existing[id]["status"] not in [ST_LIBRE, ST_OFICIAL, ST_DUPLICADA]:
            if ("nameEN" in existing[id] and existing[id]["nameES"] == ""):
                t = trans(existing[id]["nameEN"])
                if t is not None:
                    existing[id]["nameES"] = t
                    existing[id]["status"] = ST_AUTO_TRAD
                    updated = True
            if ("descrEN" in existing[id] and existing[id]["descrES"] == ""):
                t = trans(existing[id]["descrEN"])
                if t is not None:
                    existing[id]["descrES"] = t
                    existing[id]["status"] = ST_AUTO_TRAD
                    updated = True
            if "listsEN" in existing[id]:
                for k in existing[id]["listsEN"]:
                    temp = existing[id]["listsES"][k]
                    t = transL(existing[id]["listsEN"][k])
                    if t is not None and existing[id]["listsEN"][k] != [] and temp == []:
                        existing[id]["listsES"][k] = t
                        existing[id]["status"] = ST_AUTO_TRAD
                        updated = True
            if "dataEN" in existing[id]:
                for k in existing[id]["dataEN"]:
                    temp = existing[id]["dataES"][k]
                    t = transL(existing[id]["dataEN"][k])
                    if t is not None and temp == "":
                        existing[id]["dataES"][k] = t
                        existing[id]["status"] = ST_AUTO_TRAD
                        updated = True
            if updated:
                dataToFile(existing[id], filepath)

packs = getPacks()
string_files_path = "%sstrings/*_es.txt" % ROOT
with open("%sstrings/replaces.json" % ROOT) as json_file:
    replaces = json.load(json_file)
    
print("Loading strings")
strings = load_strings(string_files_path)

for p in packs:
  generate(p["id"])
  if "items" in p:
    generate(p["id"]+"-items")
  if "pages" in p:
    generate(p["id"]+"-pages")
