#!/usr/bin/python3
# -*- coding: utf-8 -*-

##
## Este script migra las traducciones de ancestros a herencias
## See English commit: https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/-/commit/98f77b7986a0379254fea9f2f1f81c554d780179
##
## Mensaje de rectulo: 15 de enero 12:05 (EST)
##
import json
import yaml
import os
import re

from libdata import readFolder, dataToFile, getPacks, getValue, getList, equals, print_error, print_warning
from statuses import *

ROOT="../"

# ==========================
# read all available entries
# ==========================
# [0] : existing (by id)
# [1] : existing (by name)
# [2] : errors
heritageData = readFolder("%sdata/heritages/" % (ROOT))
ancestriesData = readFolder("%sdata/ancestryfeatures/" % (ROOT))

for name in heritageData[1]:
  # look for an existing match in ancestryfeatures
  if not name in ancestriesData[1]:
    print("No match found for : %s" % name)
    exit(1)

  # merge name and description into heritage
  oldEntry = ancestriesData[1][name]
  newEntry = heritageData[1][name]

  newEntry["nameES"] = oldEntry["nameES"]
  newEntry["descrES"] = oldEntry["descrES"]
  newEntry["status"] = oldEntry["status"]

  dataToFile(newEntry, "%sdata/heritages/%s" % (ROOT, newEntry["filename"]))

exit(1)

      #

# =======================
# search deleted elements
# =======================
for id in existing:
  if not id in entries:
    filename = "%sdata/%s/%s" % (ROOT, pack_id, existing[id]['filename'])
    if existing[id]['status'] != ST_NINGUNA:
      print_warning("File cannot be safely removed! %s, please fix manually!" % filename)

    os.remove(filename)

